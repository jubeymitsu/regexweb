package com.example.regexweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegexWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(RegexWebApplication.class, args);
    }

}
